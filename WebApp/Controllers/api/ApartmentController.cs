﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Apartment")]
    public class ApartmentController : Controller
    {
        private readonly IApartmentService _apartmentService;

        public ApartmentController(IApartmentService apartmentService)
        {
            _apartmentService = apartmentService;
        }

        [HttpPost]
        public IActionResult AddApartment([FromBody]ApartmentDTO apartmentdto)
        {
            var newApartment = _apartmentService.AddNewApartment(apartmentdto);

            return CreatedAtAction("GetApartmentById", new { id = newApartment.ApartmentId }, apartmentdto);
        }

        [HttpGet]
        public List<ApartmentDTO> GetAllApartments()
        {
            return _apartmentService.GetAllApartments();
        }

        [HttpGet("{apartmentId:int}")]
        public IActionResult GetApartmentById(int apartmentId)
        {
            var apartment = _apartmentService.GetApartmentById(apartmentId);
            if (apartment == null) return NotFound();
            return Ok(apartment);
        }

        [HttpGet("{personName}")]
        [Route("search/{personName}")]
        public IActionResult GetApartmentByPersonName(string personName)
        {
            var apartment = _apartmentService.GetApartmentsByPersonName(personName);
            if (apartment == null) return NotFound();
            return Ok(apartment);
        }

        [HttpGet("{buildingId:int}")]
        [Route("bybuilding/{buildingId}")]
        public List<ApartmentDTO> GetApartmentsInBuilding(int buildingId)
        {
            return _apartmentService.GetApartmentsByBuildingId(buildingId);
    }
        [HttpDelete("{apartmentId:int}")]
        public IActionResult DeleteBuilding(int apartmentId)
        {

            var apartment = _apartmentService.GetApartmentById(apartmentId);
            if (apartment == null) return NotFound();
            _apartmentService.DeleteApartment(apartmentId);
            return NoContent();

        }

        [HttpPut("{apartmentId:int}")]
        public IActionResult UpdateApartment(int apartmentId, [FromBody]ApartmentDTO apartmentdto)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);
            var apartment = _apartmentService.UpdateApartment(apartmentId, apartmentdto);
            if (apartment == null) return NotFound();
            return Ok(apartment);
        }

        [HttpPut("{apartmentId:int}")]
        [Route("logical/{apartmentId}")]
        public IActionResult LogicalDeleteApartment(int apartmentId)
        {
            _apartmentService.LogicalDeleteApartment(apartmentId);
            return NoContent();
        }

    }
   
}