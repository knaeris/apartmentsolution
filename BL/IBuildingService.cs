﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public interface IBuildingService
    {
        void LogicalDeleteBuilding(int id);
        List<BuildingDTO> GetAllBuildings();

        BuildingDTO GetBuildingById(int Id);

        BuildingDTO AddNewBuilding(BuildingDTO newBuilding);

        BuildingDTO UpdateBuilding(int id, BuildingDTO building);

        List<BuildingDTO> GetBuildingsByName(string name);
        List<BuildingDTO> GetBuildingsByAddress(string address);
        void DeleteBuilding(int id);
    }
}
