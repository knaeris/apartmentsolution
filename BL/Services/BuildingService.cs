﻿using BL.Factories;
using DAL.App.EF;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Services
{
    public class BuildingService : IBuildingService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IBuildingFactory _buildingFactory;
    

        public BuildingService(DAL.App.Interfaces.IAppUnitOfWork uow, IBuildingFactory buildingFactory)
        {
            _uow = uow;
            
            _buildingFactory = buildingFactory;
        }

        public BuildingDTO AddNewBuilding(BuildingDTO newBuilding)
        {
            bool alreadyExist = _uow.Buildings.CheckByRegister(newBuilding.BuildingRegisterCode);
            
            if (alreadyExist)
                return null;
                
            var building = _buildingFactory.Transform(newBuilding);
            _uow.Buildings.Add(building);
            _uow.SaveChanges();
            return newBuilding;
        }

      

        public void DeleteBuilding(int id)
        {
            _uow.Buildings.Remove(id);
            _uow.SaveChanges();
        }

        public void LogicalDeleteBuilding(int id)
        {
            var building = _uow.Buildings.Find(id);
            building.isDeleted = true;
            _uow.Buildings.Update(building);
            _uow.SaveChanges();
        }

        public List<BuildingDTO> GetAllBuildings()
        {
            var buildings = _uow.Buildings.All()
                .Select(p => _buildingFactory.Transform(p))
                .ToList();

            return buildings.Where(x=>x.isDeleted == false).ToList();
        }

        public List<BuildingDTO> GetBuildingsByName(string buildingName)
        {
           
            return _uow.Buildings.All().Where(b => b.BuildingName.Contains(buildingName)).Select(b => BuildingDTO.CreateFromDomain(b)).ToList();
        }

        public BuildingDTO GetBuildingById(int buildingId)
        {
            var building= _buildingFactory.TransFromWithApartments(_uow.Buildings.Find(buildingId));
            if (building.isDeleted == true) return null;
            return building;
        }

        public List<BuildingDTO> GetBuildingsByAddress(string address)
        {
           
            return _uow.Buildings.All()
                .Where(b => b.BuildingAddress.Contains(address))
                .Select(b => BuildingDTO.CreateFromDomain(b))
                .Where(x => x.isDeleted == false)
                .ToList();
        }

        public BuildingDTO UpdateBuilding(int id, BuildingDTO building)
        {
            var oldBuilding = _buildingFactory.Transform(building);
            oldBuilding.BuildingId = id;
            if (oldBuilding.isDeleted == true) return null;
            _uow.Buildings.Update(oldBuilding);
            _uow.SaveChanges();
            return building;
        }
    }
}
