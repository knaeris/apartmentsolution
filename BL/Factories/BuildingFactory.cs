﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public interface IBuildingFactory
    {
        BuildingDTO Transform(Building p);
        Building Transform(BuildingDTO dto);
        BuildingDTO TransFromWithApartments(Building p);
    }

    public class BuildingFactory : IBuildingFactory
    {
        public BuildingDTO Transform(Building p)
        {
            return new BuildingDTO
            {
                BuildingName = p.BuildingName,
                BuildingAddress = p.BuildingAddress,
                BuildingId = p.BuildingId,
                TimeCreated = p.TimeCreated,
                BuildingRegisterCode = p.BuildingRegisterCode,
                isDeleted = p.isDeleted


            };
        }

        public Building Transform(BuildingDTO dto)
        {
            return new Building()
            {
                BuildingName = dto.BuildingName,
                BuildingAddress = dto.BuildingAddress,
                BuildingId = dto.BuildingId,
                TimeCreated = dto.TimeCreated,
                BuildingRegisterCode = dto.BuildingRegisterCode,
                isDeleted = dto.isDeleted
            };
        }

        public BuildingDTO TransFromWithApartments(Building p)
        {
            var dto = Transform(p);
            if (dto == null) return null;

            dto.Apartments = p?.Apartments?
                .Select(c => ApartmentDTO.CreateFromDomain(c)).ToList();
            return dto;
        }
    }
}
