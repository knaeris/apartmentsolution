﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// https://www.upload.ee/files/8007143/ContactSolution.zip.html

namespace Domain
{
    public class Building
    {
        public int BuildingId { get; set; }

        [MaxLength(128)]
        [Required]
        public string BuildingName { get; set; }
        [MaxLength(128)]
        [Required]
        public string BuildingAddress { get; set; }
        [MaxLength(128)]
        [Required]
        public string BuildingRegisterCode { get; set; }
        
        public DateTime TimeCreated { get; set; }
        public bool isDeleted { get; set; }
        public virtual List<Apartment> Apartments { get; set; } = new List<Apartment>();

        
    }
}
