﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Apartment
    {
        public int ApartmentId { get; set; }

        [MaxLength(128)]
        [Required]
        public string PersonName { get; set; }
        [MaxLength(128)]
        [Required]
        public int NumberOfRooms { get; set; }
        [MaxLength(128)]
        [Required]
        public int SqMeters { get; set; }
        public DateTime TimeCreated { get; set; }
        
        public string Description { get; set; }
        [Required]
        public int BuildingId { get; set; }
        public virtual Building Building { get; set; }

        public bool isDeleted { get; set; }


    }
}
