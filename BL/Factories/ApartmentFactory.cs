﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public interface IApartmentFactory
    {
        ApartmentDTO Transform(Apartment c);
        Apartment Transform(ApartmentDTO dto);
        
    }

    public class ApartmentFactory : IApartmentFactory
    {
        public ApartmentDTO Transform(Apartment c)
        {
            return new ApartmentDTO
            {

                ApartmentId = c.ApartmentId,
                PersonName = c.PersonName,
                NumberOfRooms = c.NumberOfRooms,
                SqMeters = c.SqMeters,
                TimeCreated = c.TimeCreated,
                Description = c.Description,
                BuildingId = c.BuildingId,
                isDeleted = c.isDeleted


            };
        }

        public Apartment Transform(ApartmentDTO dto)
        {
            return new Apartment()
            {
                ApartmentId = dto.ApartmentId,
                PersonName = dto.PersonName,
                NumberOfRooms = dto.NumberOfRooms,
                SqMeters = dto.SqMeters,
                TimeCreated = dto.TimeCreated,
                Description = dto.Description,
                BuildingId = dto.BuildingId,
                isDeleted = dto.isDeleted

            };
        }

       
    }
}

