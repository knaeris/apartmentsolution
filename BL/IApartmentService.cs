﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL
{
   public interface IApartmentService
    {
        void LogicalDeleteApartment(int apartmentId);
        ApartmentDTO AddNewApartment(ApartmentDTO newApartment);
        List<ApartmentDTO> GetApartmentsByBuildingId(int buildingId);

        List<ApartmentDTO> GetApartmentsByPersonName(string personName);
        ApartmentDTO GetApartmentById(int id);
        List<ApartmentDTO> GetAllApartments();
        void DeleteApartment(int apartmentId);
        ApartmentDTO UpdateApartment(int apartmentId, ApartmentDTO apartmentdto);
    }
}
