﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces.Repositories
{
    public interface IBuildingRepository : IRepository<Building>
    {
        bool Exists(int id);
        bool CheckByRegister(string register);

    }
}
