﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL
{
    public class ApartmentDTO
    {
        public int ApartmentId { get; set; }
        [MaxLength(128)]
        [Required]
        public string PersonName { get; set; }
        [Required]
        public int NumberOfRooms { get; set; }
        [Required]
        public int SqMeters { get; set; }
        public DateTime TimeCreated { get; set; }
        public string Description { get; set; }
        [Required]
       public int BuildingId { get; set; }
         public bool isDeleted { get; set; }

        public static ApartmentDTO CreateFromDomain(Apartment ct)
        {
            if (ct == null) return null;

            return new ApartmentDTO()
            {
                ApartmentId = ct.ApartmentId,
                PersonName = ct.PersonName,
                NumberOfRooms = ct.NumberOfRooms,
                SqMeters = ct.SqMeters,
                TimeCreated=ct.TimeCreated,
                Description=ct.Description,
                BuildingId=ct.BuildingId,
                isDeleted=ct.isDeleted
            };
        }
    }
}
