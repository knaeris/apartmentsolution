﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
   
    [Route("api/Building")]
    public class BuildingController : Controller
    {
        private readonly IBuildingService _buildingService;

        public BuildingController(IBuildingService buildingService)
        {
            _buildingService = buildingService;
        }


        [HttpGet]
        public List<BuildingDTO> GetBuildings() => _buildingService.GetAllBuildings();
        

        [HttpGet("{buildingId:int}")]
        public IActionResult GetBuildingById(int buildingId)
        {
            var building = _buildingService.GetBuildingById(buildingId);
            if (building == null) return NotFound();
            return Ok(building);
        }

        [HttpGet("{buildingAddress}")]
        [Route("search/{buildingAddress}")]
        public List<BuildingDTO> GetBuildingByAddress(string buildingAddress)
        {
            return _buildingService.GetBuildingsByAddress(buildingAddress);
        }
        [HttpGet("{buildingName}")]
        [Route("searchbyname/{buildingName}")]
        public List<BuildingDTO> GetBuildingByName(string buildingName)
        {
            return _buildingService.GetBuildingsByName(buildingName);
        }

        [HttpPost]
        public IActionResult AddBuilding([FromBody]BuildingDTO buildingdto)
        {
            if (!ModelState.IsValid) return BadRequest();
            var newBuilding = _buildingService.AddNewBuilding(buildingdto);
            return CreatedAtAction("GetBuildingById", new { id =newBuilding.BuildingId  }, buildingdto);
        }

        [HttpPut("{buildingId:int}")]
        public IActionResult UpdateBuilding(int buildingId, [FromBody]BuildingDTO buildingdto)
        {
           
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var building = _buildingService.UpdateBuilding(buildingId, buildingdto);
            if (building == null) return NotFound();
            return Ok(building);
        }

        [HttpDelete("{buildingId:int}")]
        public IActionResult DeleteBuilding(int buildingId)
        {
           
            var building = _buildingService.GetBuildingById(buildingId);
            if (building == null) return NotFound();
            _buildingService.DeleteBuilding(buildingId);
            return NoContent();

        }

        [HttpPut("{buildingId:int}")]
        [Route("logical/{buildingId}")]
        public IActionResult LogicalDeleteBuilding(int buildingId)
        {
            _buildingService.LogicalDeleteBuilding(buildingId);
            return NoContent();
        }
    }
}