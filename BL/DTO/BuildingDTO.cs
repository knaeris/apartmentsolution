﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BL
{
    public class BuildingDTO
    {
        public int BuildingId { get; set; }
        [MaxLength(128)]
        [Required]
        public string BuildingName { get; set; }
        [MaxLength(128)]
        [Required]

        public string BuildingAddress { get; set; }
        [MaxLength(128)]
        [Required]
        public string BuildingRegisterCode { get; set; }
        public DateTime TimeCreated { get; set; }
        public virtual List<ApartmentDTO> Apartments { get; set; }
        public bool isDeleted { get; set; }


        public static BuildingDTO CreateFromDomain(Building p)
        {
            if (p == null) return null;

            return new BuildingDTO()
            {
                BuildingName = p.BuildingName,
                BuildingAddress = p.BuildingAddress,
                BuildingId = p.BuildingId,
                TimeCreated = p.TimeCreated,
                BuildingRegisterCode = p.BuildingRegisterCode,
                isDeleted = p.isDeleted

            };
        }

        public static BuildingDTO CreateFromDomainWithApartments(Building p)
        {
            var building = CreateFromDomain(p);
            if (building == null) return null;

            building.Apartments = p?.Apartments?
                .Select(c => ApartmentDTO.CreateFromDomain(c)).ToList();
            return building;

        }


    }
}
