﻿using BL.Factories;
using DAL.App.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Services
{
    public class ApartmentService : IApartmentService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IApartmentFactory _apartmentFactory;

        public ApartmentService(DAL.App.Interfaces.IAppUnitOfWork uow, IApartmentFactory apartmentFactory)
        {
            _uow = uow;
            _apartmentFactory = apartmentFactory;
        }

        public ApartmentDTO AddNewApartment(ApartmentDTO apartmentdto)
        {

            var apartment = _apartmentFactory.Transform(apartmentdto);
            _uow.Apartments.Add(apartment);
            _uow.SaveChanges();
            return apartmentdto;
        }

        public List<ApartmentDTO> GetAllApartments()
        {
            return _uow.Apartments.All()
                .Select(c => _apartmentFactory.Transform(c)).Where(x => x.isDeleted == false)
                .ToList();
        }

        public ApartmentDTO GetApartmentById(int apartmentId)
        {
           return _apartmentFactory.Transform(_uow.Apartments.Find(apartmentId));
        }

        public List<ApartmentDTO> GetApartmentsByPersonName(string personName)
        {
            return _uow.Apartments.All()
                .Where(a => a.PersonName.Contains(personName))
                .Select(a => ApartmentDTO.CreateFromDomain(a))
                .Where(x=>x.isDeleted==false)
                .ToList();
        }

        public List<ApartmentDTO> GetApartmentsByBuildingId(int buildingId)
        {

            return _uow.Apartments.All()
                .Where(p => p.BuildingId == buildingId)
                .Select(b => ApartmentDTO.CreateFromDomain(b))
                .Where(x => x.isDeleted == false)
                .ToList();
        }

        public void DeleteApartment(int apartmentId)
        {
            _uow.Apartments.Remove(apartmentId);
            _uow.SaveChanges();    
        }

        public ApartmentDTO UpdateApartment(int apartmentId, ApartmentDTO apartmentdto)
        {
            var oldApartment = _apartmentFactory.Transform(apartmentdto);
            oldApartment.ApartmentId = apartmentId;
            if (oldApartment.isDeleted == true) return null;
            _uow.Apartments.Update(oldApartment);
            _uow.SaveChanges();
            return apartmentdto;
        }

        public void LogicalDeleteApartment(int apartmentId)
        {
            var apartment = _uow.Apartments.Find(apartmentId);
            apartment.isDeleted = true;
            _uow.Apartments.Update(apartment);
            _uow.SaveChanges();
        }
    }
}
